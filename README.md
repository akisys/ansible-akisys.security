akisys.security
===============

Inspired by geerlingguy.security.

This role shall provide a minimum set of security settings for remote controlled
(SSH-based) systems.

Requirements
------------

None.

Role Variables
--------------

> SUDO permission deployment

* `sudo_deploy_permissions`
  * Boolean switch to control SUDO permission deployment
* `sudo_accounts_passwordless`
  * List of usernames for password-less SUDO
* `sudo_accounts_passworded`
  * List of usernames for password-based SUDO
* `sudo_accounts_revoked`
  * List of usernames not allowed to have SUDO permissions
* `sudo_groups_passwordless`
  * List of groupnames with members allowed to have password-less SUDO
* `sudo_groups_passworded`
  * List of groupnames with members allowed to have password-based SUDO
* `sudo_groups_revoked`
  * List of groupnames with members not allowed to have SUDO permissions

> SSH config deployment

* `sshd_base_security_enabled`
  * Boolean switch to control SSHd config item deployment
* `sshd_port`
  * Integer value of the listening SSH port
* `sshd_listen_address`
  * String for the SSH listening address
  * Defaults to `'{{ ansible_default_ipv4.address }}'`
* `sshd_config_items_override`
  * Extendable dict to override default sshd config items
  * E.g.
  ```
  sshd_config_items_override:
    port:
      state: absent
  ```

> SSH ACL deployment

* `sshd_deploy_access_permissions`
  * Boolean switch to control SSH access restriction deployment
* `sshd_access_allowusers`
  * List of usernames allowed to SSH to the machine
* `sshd_access_denyusers`
  * List of usernamed not allowed to SSH to the machine
* `sshd_access_allowgroups`
  * List of groupnames with members allowed to SSH to the machine
* `sshd_access_denygroups`
  * List of groupnames with members not allowed to SSH to the machine

> Homedir base security

* `homedir_sec_enabled`
  * Boolean switch to control homedir security deployment
* `homedir_sec_home_mode`
  * Octal value for the enforced homedir unix permission mode
  * Default: `0700`

Dependencies
------------

None.

Example Playbook
----------------

See [Molecule Playbook](molecule/default/playbook.yml)

License
-------

[MIT License](LICENSE)

Author Information
------------------

Written by Alexander Kuemmel.

Testing
-------

1. `$ pipenv install`
2. `$ pipenv run molecule test`
